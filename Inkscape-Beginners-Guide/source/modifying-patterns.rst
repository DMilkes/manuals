******************
Modifying Patterns
******************

A pattern can easily be rotated, enlarged or shrunk. When the node tool
is active, an interesting trio will appear in the top left corner:

-  a **cross**, which you can use to move the pattern
-  a **white, circular handle** that allows you to rotate the pattern.
-  a **white, square-shaped handle**, which enlarges and shrinks the
   pattern.

When you grab these handles with the mouse and drag them, they will
affect the pattern, and the result will immediately be visible.

.. figure:: images/pattern02.png
    :alt: Vertical stripes pattern in an octopus shape.
    :class: screenshot

    Standard pattern with vertical stripes.

.. figure:: images/pattern03.png
    :alt: Rotated stripes pattern in an octopus shape.
    :class: screenshot

    The pattern has been rotated, using the circular handle.

.. figure:: images/pattern04.png
    :alt: Enlarged stripes pattern in an octopus shape.
    :class: screenshot

    The pattern has been enlarged, by dragging the square handle.
