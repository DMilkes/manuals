**********************
Squares and Rectangles
**********************

|Icon for Rectangle Tool| :kbd:`F4` or :kbd:`R`

To draw a rectangle:

#. Select the Rectangle tool from the toolbox on the left.
#. Click and drag the mouse diagonally, using the same motion as when dragging a
   selection box.

The rectangle will appear immediately after you release
the mouse button. To draw a perfect square, hold down :kbd:`Ctrl` while
click-dragging with the mouse.

The square-shaped handles can be used to change the rectangle's size.
However, when you only want to change either the height or the width of
the rectangle, it's hard to control with the mouse alone. Here you can
use :kbd:`Ctrl` to restrict the size change to either width or height
of the rectangle.

.. figure:: images/squares_resizing.png
    :alt: Resize rectangles with the square-shaped handles
    :class: screenshot

    When you hold down :kbd:`Ctrl` while dragging the square-shaped handles, it is easy to limit the change in the rectangle's size to a single direction.

The circle-shaped handles are for rounding the corners. Grab a circle
handle and move it just a tiny bit. You'll see that all four corners
will be rounded. Additionally, a second circular handle has appeared now. Each
handle changes the amount of rounding in one direction. It is not
possible to only round a single corner of the rectangle independant from
the others. They will all change together.

.. figure:: images/squares_rounding.png
    :alt: Moving the circle handles rounds the corners.
    :class: screenshot

    Moving the circle handles rounds the corners. Each circle handle changes the radii in a different direction.

.. figure:: images/squares_corner_radii.png
    :alt: The rounding is based on the radius of an imaginary circle
    :class: screenshot

    The "Rx" and "Ry" values on the control bar, determine the radius of the imaginary circle which the rounding is based upon.

To restore the initial, sharp-cornered shape of a rectangle or square,
click on the far right icon in the tool control bar |Rectangle Tool sharp corners|.
This is very useful, when you're still learning how to master the
usage of the circular handles!

When you need to draw a rectangle with accurate dimensions, you can use
the tool controls bar:

- the field labelled :guilabel:`W` is for the width;
- the field labelled :guilabel:`H` is for the height;
- the :guilabel:`Rx` and :guilabel:`Ry` fields define the rounding radius for
  the corners.

The dropdown behind each number entry field allows you to select the unit you
need for your rectangle.

.. figure:: images/rect-tool-controls-bar_win10.png
    :alt: The controls bar of the rectangle tool
    :class: screenshot

.. |Icon for Rectangle Tool| image:: images/icons/draw-rectangle.*
   :class: header-icon
.. |Rectangle Tool sharp corners| image:: images/icons/rectangle-make-corners-sharp.*
   :class: inline
